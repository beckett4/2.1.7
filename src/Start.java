//Dependencies
import model.Model;
import view.TodoListFrame;
import javax.swing.*;

//Rez
//Start here
public class Start {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(()
                -> new TodoListFrame(new Model()));
    }
}
