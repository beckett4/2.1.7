//dependencies
package model;
import java.util.Optional;
//Beckett, Huang, Remy

//CatchingBadData
//Based on the lecture stuff
//Please don't mess with it too much I based it mostly off temperaturerecording because I'm not too knowledgeable
// at error catchment.

public class CatchingBadData<T> {
    private T obj;
    private ErrorCatch error;

    //When we can return an object that we're happy with
    public CatchingBadData(T obj) {
        this.obj = obj;
        error = new ErrorCatch();
    }
    //When we can return an object we have an error for
    public CatchingBadData(ErrorCatch error) {
        this.error = error;
        this.obj = null; //Make sure we dont save the bad object
    }


    //Errol's model contains this and for some reason it makes everything work.
    public Optional<T> get() {
        return Optional.ofNullable(obj);
    }
    public T getObj() {
        return obj;
    }

    //is data actually present in this zone? Don't delete, my other added validation relied on this...
    //...even if we haven't got proper validation at the moment
    public boolean isData() {
        return obj != null;
    }
    public boolean isEmpty() {
        return obj == null;
    }
    public static String isData(String string, String errorMessage) {
        if (string == null || string.trim().length() == 0) { return errorMessage; }
        return null;
    }

}
