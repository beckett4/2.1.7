//dependencies
package model;
import javax.swing.*;
//Huang, Remy

//Inputs should be doubles as opposed to ints as this allows for greater accuracy
public class EnsureDoubles extends InputVerifier {
    @Override
    public boolean verify(JComponent input) {
        String inputText = ((JTextField) input).getText();
        try {
            Double.parseDouble(inputText); //can the input be read as a double?
        }
        catch (NumberFormatException ex) { //no, don't let it go through
            return false;
        }
        return true; //yes, it's fine let it go through
    }
}