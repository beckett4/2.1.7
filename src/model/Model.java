//dependencies... this is the big one
package model;
import model.data.AbstrNode;
import model.data.Task;
import model.data.Project;
import model.data.UrgencyAdd;
import model.scaleunused.HighAcc;
import model.scaleunused.MediumAcc;
import model.scaleunused.LowAcc;
import model.scaleunused.UrgencyAccuracy;
import view.SelectedNodeChanged;
import javax.swing.tree.TreeNode;
import java.util.Calendar;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.Vector;
import javax.swing.*;
import javax.swing.tree.TreeModel;
//Beckett, Remy, Ugonna, Huang, Rez


//declaring scales for accuracy
public class Model {
    public static UrgencyAccuracy HIGH_SCALE = new HighAcc();
    public static UrgencyAccuracy LOW_SCALE = new LowAcc();
    public static UrgencyAccuracy MED_SCALE = new MediumAcc();
    public static UrgencyAccuracy[] ACCURACY_GAUGE = {HIGH_SCALE, LOW_SCALE, MED_SCALE};

    //This is the only time the root should be visible for the purposes of selecting a parent project
    private Project rootProject =
            //default value is no parent, high scale
            Project.create("Select parent", " ", HIGH_SCALE).getObj();

    //Don't remove or jtree will get angry
    private MyTreeModel myTree = new MyTreeModel(rootProject);
    private TreeMap<String, Project> projects = new TreeMap<>();
    private TreeMap<String, Task> projs = new TreeMap<>();
    private HashSet<String> names = new HashSet<>();

    //Create a list of strings for errors. Can be removed later.
    private DefaultListModel<String> errorModel = new DefaultListModel<>();
    private TreeNode selectedNode;
    private Vector<SelectedNodeChanged> selectionListeners = new Vector<>();
    public Model() {
        projects.put(" ", rootProject);
    }

    //get the data for the tree and return it
    public TreeModel getTreeModel() {
        return myTree;
    }

    //Make sure we don't duplicate names or there could be issues
    //This bool was meant to register whether or not a name is in use but it's not 100% there
    private boolean checkNameUse(String name, AbstrNode.AnyNode anyNode) {
        ErrorCatch errorCatch = new ErrorCatch(CatchingBadData.isData(name,
                "No name.")); //Catchall, leave this in.
        if (!errorCatch.isEmpty()) {
            errorModel.addAll(errorCatch);
            return true;
        }
        if (names.contains(name)) { //if the list of name contains this entered name already, then print
            System.out.println("Duplication error."); //if the name isn't unique, this prints to console
            return true;
        }
        return false;  //otherwise don't worry about it
    }

    //Stops bad stuff from going to tree branches please don't remove this bit
    public void addNode(String name, String description, Project parent, UrgencyAccuracy defaultScale) {
        errorModel.clear();
        CatchingBadData<Project> projectValid = Project.create(name, description, parent == null ?
                rootProject : parent, defaultScale); // is input valid for all these and not null
        if (projectValid.isData()) { //has it been confirmed ok
            final Project project = projectValid.getObj(); //project is a final now as we don't want to edit it
            projects.put(project.getName(), project);
            names.add(project.getName());
            myTree.executeNewNode(project); //all is good, so execute adding to the tree
        } else {
            System.out.println("Validation error at addNode."); // if it somehow messes up this will go to console
        }
    }

    //As above but for tasks
    public void addTask(String name, String description, Project parent, UrgencyAccuracy defaultScale) { //new task
        errorModel.clear(); //flush any errors from console
        if (checkNameUse(name, AbstrNode.AnyNode.TASK)) return; //is the name in use?
        CatchingBadData<Task> taskBad = Task.create(name, description,
                51, defaultScale, parent); //does it have valid inputs for all of these?
        if (taskBad.isData()) {
            final Task task = taskBad.getObj();
            projs.put(task.getName(), task);
            names.add(task.getName());
            myTree.executeNewNode(task); //if it passed, you can add it to the tree
        } else {
            System.out.println("Validation error at addTask."); //if it messes up this will go to console
        }

    }

    //for validating dates input
    public void addData(Calendar date, int hour, int minutes, Task task,
                        UrgencyAccuracy urgencyAccuracy, String urgency) {
        errorModel.clear();
        CatchingBadData<UrgencyAdd> urgValid =
                //what a valid one should look like
                UrgencyAdd.create(date, hour, minutes, task, urgencyAccuracy, urgency);
        if (urgValid.isData()) { //if what we have is a valid one
            final UrgencyAdd dataNode = urgValid.getObj();
            myTree.executeNewNode(dataNode); //put it on the tree
        } else {
            System.out.println("Data entry error at addData."); //print to console
        }
    }

    //references that need to be public, don't delete
    public TreeMap<String, Project> getProjects() {
        return projects;
    }
    public ComboBoxModel<Project> getTaskCombBoxMod() {
        return new ProjsComboModel(this);
    }
    public ComboBoxModel<Task> getProjsCombBoxMod() {
        return new TasksComboModel(this);
    }
    Task[] getProjs() {
        return projs.values().toArray(new Task[0]);
    }

    //set selected item and notify all listeners
    public void setSelected(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
        for (SelectedNodeChanged treeModListenr : selectionListeners) {
            treeModListenr.selectionChanger(selectedNode);
        }
    }

    public boolean deleteTreeEntry() {

        AbstrNode parent = (AbstrNode) selectedNode.getParent();
        final boolean success = parent.deleteChild(selectedNode);
        myTree.executeNewNode(parent);
        return success;
    }

}
