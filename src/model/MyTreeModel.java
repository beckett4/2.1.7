//dependencies
package model;
import model.data.Project;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Vector;
//Ugonna, Beckett

//constructing the tree!!!!!
public class MyTreeModel implements TreeModel {
    private final Project rootProject; //root should not be editable under any circumstance, so it's final
    private Vector<TreeModelListener> treeModelListeners = new Vector<>();
    public MyTreeModel(Project rootProject) {
        this.rootProject = rootProject;
    } //making the tree accessible

    //make sure our tree has a root, else our tree is empty and it should return -1
    @Override
    public Object getRoot() {
        return rootProject;
    }
    //get a node of a certain point in the index
    @Override
    public Object getChild(Object parent, int index) {
        return ((TreeNode) parent).getChildAt(index);
    }
    //how many children nodes are there? return this figure
    @Override
    public int getChildCount(Object parent) {
        return ((TreeNode) parent).getChildCount();
    }
    //bool returns true if this node has no children and is therefore a leaf
    @Override
    public boolean isLeaf(Object node) {
        return ((TreeNode) node).isLeaf();
    }
    //update new values appended to nodes
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
    }
    //return the index number of a child
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return ((TreeNode) parent).getIndex((TreeNode) child);
    }
   //adds listener DO NOT REMOVE
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.addElement(l);
    }
   //remove listener when done
    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.remove(l);
    }

    //triggered when new nodes are added
    public void executeNewNode(TreeNode addProj) {
        ArrayList<TreeNode> path = new ArrayList<>();
        path.add(rootProject);
        for (TreeNode parent = addProj.getParent();
             parent != rootProject;
             parent = parent.getParent()) {
            path.add(1,parent);
        }
        final int[] indices = {addProj.getParent().getIndex(addProj)};
        TreeModelEvent e = new TreeModelEvent(this, path.toArray(), indices,
                new Object[] {addProj});
        for (TreeModelListener tml : treeModelListeners) {
            tml.treeStructureChanged(e);
        }
    }
}
