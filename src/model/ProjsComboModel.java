//dependencies
package model;
import model.data.Project;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
//Ugonna, Huang, Beckett

//let's contruct the project selection combobox!
public class ProjsComboModel implements ComboBoxModel<Project>, TreeModelListener {
    private Model model; //incorporates the model
    private Project[] projects; //incorporates the project list
    private Vector<ListDataListener> listofListeners = new Vector<>();
    private Project selectedItem;
    public ProjsComboModel(Model model) {
        this.model = model;
        //incorporated the tree
        MyTreeModel myTreeModel = (MyTreeModel) model.getTreeModel();
        myTreeModel.addTreeModelListener(this);
        projects = model.getProjects().values().toArray(new Project[0]);
        Arrays.sort(projects);
        setSelectedItem(projects[0]);
    }

   //Sets selected item
    //DONT MESS WITH THIS
    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (Project) anItem;
    }
    // Return selected item
    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
    //Return list length
    @Override
    public int getSize() {
        return model.getProjects().size();
    }
    //Return requested index number from array
    @Override
    public Project getElementAt(int index) {
        return projects[index];
    }

    //add listener for update
    @Override
    public void addListDataListener(ListDataListener l) {
        listofListeners.add(l);
    }
    //remove aforementioned listener once complete
    @Override
    public void removeListDataListener(ListDataListener l) {
        listofListeners.remove(l);
    }
    //execute changes to tree
    private void executeTreeChanges() {
        ListDataEvent event = new ListDataEvent(projects, ListDataEvent.CONTENTS_CHANGED, 0,
                projects.length); //get new contents
        for (ListDataListener listDataListener : listofListeners) { //update for each listener in list
            listDataListener.contentsChanged(event);
        }
    }
    //update when a node has been edited
    @Override
    public void treeNodesChanged(TreeModelEvent e) {
        treeStructureChanged(e);
    }
    //update when node is added
    @Override
    public void treeNodesInserted(TreeModelEvent e) {
        treeStructureChanged(e);
    }
    //update when node removed
    @Override
    public void treeNodesRemoved(TreeModelEvent e) {
        treeStructureChanged(e);
    }
    //update when change to structure of tree
    @Override
    public void treeStructureChanged(TreeModelEvent e) {
        projects = model.getProjects().values().toArray(new Project[0]);
        Arrays.sort(projects);
        setSelectedItem(selectedItem);
        executeTreeChanges();
    }
}