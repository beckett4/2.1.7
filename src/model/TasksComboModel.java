//dependencies
package model;
import model.data.Task;
import javax.swing.*;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
//Ugonna, Huang, Beckett

//lets construct the tasks combobox
public class TasksComboModel implements ComboBoxModel<Task>, TreeModelListener {
    private final Model model; //incorporates the model
    private Task[] tasks;  //incorporates list of tasks
    private Vector<ListDataListener> listOfListeners = new Vector<>(); //needs access to listener
    private Task selectedItem; //needs to specifically listen to selected item

    public TasksComboModel(Model model) {
        this.model = model;
        //incorporates the tree
        MyTreeModel myTreeModel = (MyTreeModel) model.getTreeModel(); //get the tree
        myTreeModel.addTreeModelListener(this); //add listener to tree
        tasks = model.getProjs(); //receive all the projects
        Arrays.sort(tasks); //Makes a nice list of the tasks to choose from
    }

    //Selects an item
    //DONT DELETE AS THE LISTENERS NEED THIS LINE HERE
    @Override
    public void setSelectedItem(Object itemSelected) {
        selectedItem = (Task) itemSelected;
    }
    //Returns selection
    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
    //Returns length of list
    @Override
    public int getSize() {
        return tasks.length;
    }

    //Returns a specific index value for search purposes
    @Override
    public Task getElementAt(int index) {
        return tasks[index];
    }

    //Listener for changes DO NOT DELETE
    @Override
    public void addListDataListener(ListDataListener l) {
        listOfListeners.add(l);
    }
    //THIS CODE WILL REMOVE THE LISTENER WHEN DONE
    @Override
    public void removeListDataListener(ListDataListener l) {
        listOfListeners.remove(l);
    }
    //Update the tree when stuff has changed
    private void executeTreeChanges() {
        ListDataEvent event = new ListDataEvent(tasks, ListDataEvent.CONTENTS_CHANGED, 0,
                tasks.length); //get the new amount of tasks
        for (ListDataListener listDataListener : listOfListeners) {
            listDataListener.contentsChanged(event); //for each listener in the list, update contents
        }
    }

    //Override when a node has changed
    @Override
    public void treeNodesChanged(TreeModelEvent e) {
        treeStructureChanged(e);
    }
    //Override when a node is added
    @Override
    public void treeNodesInserted(TreeModelEvent e) {
        treeStructureChanged(e);
    }
    //Override when a node is deleted
    @Override
    public void treeNodesRemoved(TreeModelEvent e) {
        treeStructureChanged(e);
    }
    //Override for changes to base structure of tree
    @Override
    public void treeStructureChanged(TreeModelEvent e) {
        tasks = model.getProjs(); //get the projects
        Arrays.sort(tasks); //resort them
        setSelectedItem(selectedItem); //refresh selected item
        executeTreeChanges(); //carry out changes
    }
}
