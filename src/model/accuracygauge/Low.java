//dependency
package model.accuracygauge;
//Beckett

//defining what low is
public class Low implements Accuracy {
    private double accuracy;
    //holds a low value which cant be changed
    public Low(double accuracy) {
        this.accuracy = accuracy;
    }


    //Ignore the other stuff in accuracy interface as we did not get chance to create a conversion scale
    @Override
    public double getHigh() {
        return 0;
    }
    @Override
    public double getLow() {
        return 0;
    }
    @Override
    public double getMed() {
        return 0;
    }
}
