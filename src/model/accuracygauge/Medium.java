//dependency
package model.accuracygauge;
//Beckett

//defining what medium is
public class Medium implements Accuracy {
    private double accuracy;
    //holds a medium value
    public Medium(double accuracy) {
        this.accuracy = accuracy;
    }


    //Ignore the other stuff in accuracy interface as we did not get chance to create a conversion scale
    @Override
    public double getHigh() {
        return 0;
    }
    @Override
    public double getLow() {
        return 0;
    }
    @Override
    public double getMed() {
        return 0;
    }

}