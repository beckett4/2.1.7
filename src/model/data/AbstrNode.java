//dependencies
package model.data;
import model.scaleunused.UrgencyAccuracy;
import javax.swing.tree.TreeNode;
import java.util.stream.Stream;
//Huang

//The abstract class for creating any type of node in the tree.
public abstract class AbstrNode implements TreeNode { //an abstract node contains...
    //These cannot be static otherwise the tree doesn't construct itself properly.
    protected String name; //a name
    protected String desc; //a description
    protected UrgencyAccuracy defaultScale; //a scale of accuracy
    protected Project parent; //potentially a parent

    //the enum represents a list of constants. in our case a node can be a project or a task.
    public enum AnyNode {
        PROJ("Project"),
        TASK("Task");
        String name;
        AnyNode(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }
    }

    //Construct fields for a node. A node is public because other stuff access it frequently.
    public AbstrNode(String name, String desc, Project parentProject, UrgencyAccuracy urgencyAccuracy) {
        this.name = name.trim(); //the name
        this.desc = desc.trim(); //the description
        this.defaultScale = urgencyAccuracy; //the scale of accuracy chosen
        this.parent = parentProject; //the parent
        if(parent != null) {
            parent.add(this); //if there is a parent, add it, otherwise leave null
        }
    }

    //get name
    public String getName() {
        return name;
    }
    //get desc
    public String getDesc() {
        return desc;
    }
    //get parent
    public Project getParent() {
        return parent;
    }
    //get scale
    public UrgencyAccuracy scale() {
        return defaultScale;
    }
    //get stream of urgencies
    public abstract Stream<UrgencyAdd> getAccuracy();

    public abstract boolean deleteChild(TreeNode selectedItem);

    //Tree stuff
    @Override
    public boolean getAllowsChildren() {
        return true;
    }
    //this should always return true because it's always possible to add sub tasks
}
