//dependency
package model.data;
import model.scaleunused.UrgencyAccuracy;
import model.CatchingBadData;
import javax.swing.tree.TreeNode;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.stream.Stream;
//Huang

//A project is one of the things we can create from our abstract node
public class Project extends AbstrNode implements Comparable<Project> {
    private HashMap<String, AbstrNode> children = new HashMap<>();
    //projects can contain other projects and can also have null parents, hence hashmap of projs
    //make a project will no parent project (the parent is null)
    private Project(String name, String description, UrgencyAccuracy urgencyAccuracy) {
        super(name, description, null, urgencyAccuracy);
    }
    //or make a project that is inside another project
    private Project(String name, String description, Project parent, UrgencyAccuracy urgencyAccuracy) {
        super(name, description, parent, urgencyAccuracy);
    }

    //Stream of accuracies
    public Stream<UrgencyAdd> getAccuracy() {
        return (children.values()
                .parallelStream()
                .flatMap(AbstrNode::getAccuracy));
    }

    //add a child
    void add(AbstrNode project) {
        children.put(project.getName(), project);
    }

    //handles ordering within the tree
    @Override
    public int compareTo(Project o) { //compare the int to project
        return "root".equals(getName()) ? -1 : //it's -1 if there is no parent
                "root".equals(o.getName()) ? 1 : getName().compareTo(o.getName()); //or show parent
    }
    //Returns the text string next to the name of project
    @Override
    public String toString() {
        return ("ROOT".equals(getName()) ? "" : getName() + ": ") + getDesc(); //display name and description
    }

    @Override
    public boolean deleteChild(TreeNode selectedItem) {
        return false;
    }

    //Tree stuff
    @Override //get child at the specified index
    public TreeNode getChildAt(int childIndex) {
        return (TreeNode) children.values().toArray()[childIndex];
    }
    @Override //get number of children
    public int getChildCount() {
        return children.size();
    }
    @Override //return true if childless
    public boolean isLeaf() {
        return children.isEmpty();
    }
    @Override //get desired index
    public int getIndex(TreeNode node) {
        Object[] projectArray = children.values().toArray();
        for (int i = 0; i < children.size(); i++) {
            if (node == projectArray[i]) {
                return i;
            }
        }
        return -1;
    }
    @Override
    public Enumeration<? extends TreeNode> children() {
        return Collections.enumeration(children.values());
    }
    public static CatchingBadData<Project> create(String name, String description, UrgencyAccuracy urgencyAccuracy) {
        return new CatchingBadData<>(new Project(name, description, urgencyAccuracy));
    }

    //For making projects that are subprojects and catching errors for this
    public static CatchingBadData<Project> create(String name, String description, Project parent, UrgencyAccuracy urgencyAccuracy) {
        return new CatchingBadData<>(new Project(name, description, parent, urgencyAccuracy));
    }
}