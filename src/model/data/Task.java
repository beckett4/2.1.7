//dependencies
package model.data;
import model.scaleunused.UrgencyAccuracy;
import model.CatchingBadData;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.stream.Stream;
//Huang, Beckett, Rez

//a task is one of our abstract node types
public class Task extends AbstrNode implements Comparable<Task> {
    private final ArrayList<UrgencyAdd> accuracy = new ArrayList<>(); //it has an accuracy

    //Construct a task
    private Task(String name, String description,
                 UrgencyAccuracy urgencyAccuracy, Project project) {
        super(name, description, project, urgencyAccuracy);
    }
    //Has an accuracy
    public void add(UrgencyAdd accuracy) {
        this.accuracy.add(accuracy);
    }
    //Has a stream of accuracies
    public Stream<UrgencyAdd> getAccuracy() {
        return accuracy.stream();
    }
    //Order objects in list
    @Override
    public int compareTo(Task o) {
        return getName().compareTo(o.getName());
    }
    //Displays the details of the task next to it in the tre
    @Override
    public String toString() {
        return getName() + ": " + getDesc();
    }

    @Override
    public boolean deleteChild(TreeNode selectedItem) {
        return accuracy.remove(selectedItem);
    }

    //Tree stuff
    @Override //get child at desire index
    public TreeNode getChildAt(int childIndex) {
        return accuracy.get(childIndex);
    }
    @Override //get number of children
    public int getChildCount() {
        return accuracy.size();
    }
    @Override //return true if childless
    public boolean isLeaf() {
        return accuracy.isEmpty();
    }
    @Override //get desired index
    public int getIndex(TreeNode node) {
        return -1;
    }
    @Override
    public Enumeration<? extends TreeNode> children() {
        return Collections.enumeration(accuracy);
    }

    //Factory method for task
    public static CatchingBadData<Task> create(String name, String description,
                                               int i, UrgencyAccuracy urgencyAccuracy, Project project) {
    //ended up not fully implementing this in time...
        return new CatchingBadData<>(new Task(name, description,
                urgencyAccuracy, project));
    }
}