//dependencies
package model.data;
import model.scaleunused.UrgencyAccuracy;
import model.accuracygauge.Accuracy;
import model.CatchingBadData; // not fully implemented
import model.ErrorCatch; // not fully implemented
import javax.swing.tree.TreeNode;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Enumeration;
//Huang, Remi

//this is the class that collates the date, time, task
//its not utilised fully due to the lack of time to implement a conversion scale for grading tasks
public class UrgencyAdd implements TreeNode {
    private Calendar date; //urgency involves a date
    private int hours; //and a hour in which the due time resides
    private Task task; //and a specified task
    protected Accuracy accuracy; //and the selected accuracy
    UrgencyAdd(Calendar date, int hours, Task task, Accuracy accuracy) {
        this.date = date;
        this.hours = hours;
        this.task = task;
        this.accuracy = accuracy;
        task.add(this);
    }
    public String getTimeString() {
        return "Due in the hour of: " + hours + ":00";
    } //return rough due time

    //Tree stuff
    @Override //return if subtasks are available to add (should always be true)
    public boolean getAllowsChildren() {
        return false;
    }
    @Override //returns if no children
    public boolean isLeaf() {
        return true;
    }
    @Override //return child at stated index
    public TreeNode getChildAt(int childIndex) {
        return null;
    }
    @Override //return no of children
    public int getChildCount() {
        return 0;
    }
    @Override //return parent
    public TreeNode getParent() {
        return task;
    }
    @Override //return index no
    public int getIndex(TreeNode node) {
        return 0;
    }
    @Override //returns children as enum
    public Enumeration<? extends TreeNode> children() {
        return null;
    }

    //This doesn't get used but don't delete it because apparently something became dependent... lol
    public String toString() {
        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(date.getTime()) + " " +
                getTimeString() ;
    }

   //Catch potential bad data and output an error code to the console
    //for dev purposes - good to know what went wrong!
    private static ErrorCatch parameterValid(Calendar date, int hour, int minutes, Task task,
                                             UrgencyAccuracy urgencyAccuracy, String urgency) {
        ErrorCatch errorCode = new ErrorCatch();
        //if the time makes no sense
        if (hour < 0 || hour > 23 || minutes < 0 || minutes > 59) {
            System.out.println("Invalid time was input");
        }
        //if there is no task selected
        if(task == null) System.out.println("Please select task");
        //if there is no accuracy selected
        if(urgencyAccuracy == null) System.out.println("Please select accuracy");
        try {
            Double.parseDouble(urgency);
        } catch (NumberFormatException e) {
            //or if accuracy is a bad input
            System.out.println("Accuracy must be number");
        }
        return errorCode;
    }

    //Construct method for rejecting or adding
    public static CatchingBadData<UrgencyAdd> create(Calendar date, int hour, int minutes,
                                                     Task task, UrgencyAccuracy urgencyAccuracy,
                                                     String urgency) {
        ErrorCatch errorCode = parameterValid(date, hour, minutes, task, urgencyAccuracy, urgency);
        if (errorCode.isEmpty()) {
            return new CatchingBadData<>(new UrgencyAdd(date, hour + minutes, task,
                    urgencyAccuracy.makeUrg(Double.parseDouble(urgency))));
        }
        return new CatchingBadData<>(errorCode);
    }
}
