//dependencies
package view;
import model.Model;
import model.data.Project;
import model.scaleunused.UrgencyAccuracy;
import java.awt.*;
//Rez

//constructing a tasks panel
public class TasksPanel extends AbstrNodePanel {
    public static final int MIN_WIDTH = 450;
    public static final int MIN_HEIGHT = 165;

    public TasksPanel(String title, Model model, int width, int height, Color colour) {
        super(title, model, Math.max(MIN_WIDTH, width), Math.max(MIN_HEIGHT, height), Color.pink);

    }


    //Override for adding nodes
    @Override
    protected void addNode(String name, String description, Project parent, UrgencyAccuracy defaultScale) {
        model.addTask(name, description, parent, defaultScale);
    }

}
